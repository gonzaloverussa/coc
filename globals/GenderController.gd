extends Node

const ICON_PATH = "res://resources/genders/"
const genders = {
    "Fighting": {
		"name": "Fighting",
        "icon": ICON_PATH + "fighting.png",
    },
    "Beat’em up": {
		"name": "Beat’em up",
        "icon": ICON_PATH + "beat_em_up.png",
    },
    "Platform": {
		"name": "Platform",
        "icon": ICON_PATH + "platform.png",
    },
    "Battle Royale": {
		"name": "Battle Royale",
        "icon": ICON_PATH + "battle_royale.png",
    },
    "Shooter": {
		"name": "Shooter",
        "icon": ICON_PATH + "shooter.png",
    },
    "Shoot’em up": {
		"name": "Shoot’em up",
        "icon": ICON_PATH + "shoot_em_up.png",
    },
    "Strategy": {
		"name": "Strategy",
        "icon": ICON_PATH + "strategy.png",
    },
    "Tactic": {
		"name": "Tactic",
        "icon": ICON_PATH + "tactic.png",
    },
    "Tower Defense": {
		"name": "Tower Defense",
        "icon": ICON_PATH + "tower_defense.png",
    },
    "Simulation": {
		"name": "Simulation",
        "icon": ICON_PATH + "simulation.png",
    },
    "Sport": {
		"name": "Sport",
        "icon": ICON_PATH + "sport.png",
    },
    "Racing": {
		"name": "Racing",
        "icon": ICON_PATH + "racing.png",
    },
    "Adventure": {
		"name": "Adventure",
        "icon": ICON_PATH + "adventure.png",
    },
    "RPG": {
		"name": "RPG",
        "icon": ICON_PATH + "rpg.png",
    },
    "Music": {
		"name": "Music",
        "icon": ICON_PATH + "music.png",
    },
    "Puzzle": {
		"name": "Puzzle",
        "icon": ICON_PATH + "puzzle.png",
    },
    "Survival": {
		"name": "Survival",
        "icon": ICON_PATH + "survival.png",
    }
}

func get_random(cant):
	if cant > 0:
		var rng = RandomNumberGenerator.new()
		var array_genders = genders.values()
		var cant_genders = array_genders.size()
		var random_genders = {}
		var random_gender
		while random_genders.size() < cant:
			rng.randomize()
			random_gender = array_genders[rng.randi_range(0,cant_genders-1)].name
			random_genders[random_gender] = genders[random_gender]
		return random_genders
	return null