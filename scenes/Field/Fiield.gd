extends Control

var card = null

func insert_card(card_to_insert):
	if can_insert():
		add_child(card_to_insert.duplicate(8))
		card = get_child(get_child_count() - 1)
		card.set_global_position(get_global_position())
		return true
	return false

func can_insert():
	return true if card == null else false