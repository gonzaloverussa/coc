extends "res://scenes/Container.gd"

func insert_item(item, cursor_pos):
	var field = get_item_under_pos(cursor_pos)
	if field != null and field.has_method("insert_card"):
		return field.insert_card(item)
	return false