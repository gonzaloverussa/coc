extends MarginContainer

onready var containers = [$VBoxContainer/CenterContainer/PlayerHand,$VBoxContainer/PlayerField/Battlefield]

var item_held = null
var item_offset = Vector2()
var last_container = null
var last_pos = Vector2()
var genderSelector = preload("res://scenes/GenderSelector/GenderSelector.tscn")

func _process(delta):
	var cursor_pos = get_global_mouse_position()
	if Input.is_action_just_pressed("ui_click"):
		#se comenzó a hacer click
		grab(cursor_pos)
	if Input.is_action_just_released("ui_click"):
		#se soltó el click
		release(cursor_pos)
	if item_held != null:
		#se tiene algo seleccionado, se lo arrastra
		item_held.rect_global_position = cursor_pos + item_offset

func grab(cursor_pos):
	var container = get_container_under_cursor(cursor_pos)
	if container != null and container.has_method("grab_item"):
		item_held = container.grab_item(cursor_pos)
		if item_held != null:
			last_container = container
			last_pos = item_held.rect_global_position
			item_offset = item_held.rect_global_position - cursor_pos
			move_child(item_held, get_child_count()) #mueve el hijo al final de los hijos del padre

func release(cursor_pos):
	if item_held != null:
		var container = get_container_under_cursor(cursor_pos)
		if container != null and container.has_method("insert_item") and container.insert_item(item_held, cursor_pos):
			#se suelta el item en un contenedor que posee el metodo insert_item y lo pudo insertar
			last_container.remove_item(item_held)
			item_held = null
			remove_child(find_node("GenderSelector",true,false))
			var instancia = genderSelector.instance()
			instancia.set_name("GenderSelector")
			add_child(instancia)
		else:
			#no se puede dejar el item ahi por lo que se lo retorna
			return_item()

func get_container_under_cursor(cursor_pos):
	for container in containers:
		if container.get_global_rect().has_point(cursor_pos):
			#se encontro la carta que esta debajo del cursor
			return container
	return null

func return_item():
	item_held.rect_global_position = last_pos
#	last_container.insert_item(item_held)
	item_held = null