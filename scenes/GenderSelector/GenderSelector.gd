extends CenterContainer

func _ready():
	var random_genders = GenderController.get_random(3)
	var circular_selector = $CircularSelector
	for gender in random_genders.values():
		var texture_rect = TextureRect.new()
		texture_rect.set_texture(load(gender.icon))
		circular_selector.add_child(texture_rect)
