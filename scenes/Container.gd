extends Control

func get_item_under_pos(cursor_pos):
	for item in self.get_children():
		if item.get_global_rect().has_point(cursor_pos):
			return item
	return null