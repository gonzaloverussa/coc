extends "res://scenes/Container.gd"

func grab_item(cursor_pos):
	var item = get_item_under_pos(cursor_pos)
	return item

func remove_item(item):
	remove_child(item)